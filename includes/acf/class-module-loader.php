<?php

class Dachcom_Page_Designer_ModuleLoader {

    /**
     * @var bool
     */
    private $usingThemosis = FALSE;

    /**
     * @var \Dachcom_Page_Designer_ModuleFactory
     */
    private $moduleFactory = NULL;

    public function __construct() {

        if( class_exists('THFWK_Themosis') ) {

            $this->usingThemosis = TRUE;

        }

        $this->moduleFactory = new Dachcom_Page_Designer_ModuleFactory();

        $this->registerAjax();
        $this->setupModuleFields();

    }

    /* AJAX HANDLER */

    private function registerAjax() {

        add_action('wp_ajax_nopriv_load_module_ajax',   array($this, 'moduleAjax') );
        add_action('wp_ajax_load_module_ajax',          array($this, 'moduleAjax') );

    }

    public function setupModuleFields() {

        if( !function_exists("register_field_group"))
            return;

        $maxColumns                 = get_option('dpd_setting-theme_column');
        $allowedFields              = get_option('dpd_setting-first_cols_allowed');
        $disableMultipleFirstRows   = get_option('dpd_setting-max_first_col_amount');

        if( empty( $allowedFields ) )
            $allowedFields = array( 12 );

        if( !is_numeric( $maxColumns ) )
            $maxColumns = 12;

        $defaultFirstRowClasses = array( );

        foreach( $allowedFields as $allowedFieldID ) {

            if( $allowedFieldID > $maxColumns)
                continue;

            $defaultFirstRowClasses[ dpd_col_int_to_string( $allowedFieldID )] = $allowedFieldID . '/' . $maxColumns;

        }

        $defaultFirstRowClassSelected = dpd_col_int_to_string( $maxColumns );

        $defaultLocationArray = array(

            array(

                array(

                    'param' => 'page_template',
                    'operator' => '==',
                    'value' => $this->usingThemosis ? 'none' : 'default',
                    'order_no' => 0,
                    'group_no' => 0,

                )
            )

        );

        //because this is much complex, you have to apply a filter, for the correct page templates.
        $locationArray = apply_filters('dachcom-page-designer/page-designer-location', $defaultLocationArray);

        $mainFieldNames = array(

            'field_page_repeater'       => 'field_page_designer',
            'field_main_column_size'    => 'field_pd_fl_column_size',
            'field_module_id'           => 'field_pd_module_id'

        );

        //if you want, you can change the main fields.
        $mainFieldNames = apply_filters('dachcom-page-designer/set-main-field-names', $mainFieldNames);

        #####
        ##### CONFIG END

        $availableModules = $this->getAllModuleFields();

        register_field_group(

            array(

                'id' => 'page_designer',
                'title' => 'Page-Designer',
                'fields' => array(

                    array(

                        'key' => $mainFieldNames[ 'field_page_repeater' ],
                        'label' => 'Spalte',
                        'name' => 'page_designer',
                        'type' => 'repeater',
                        'button_label' => 'Spalte hinzufügen',

                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'min' => 1,
                        'max' =>  $disableMultipleFirstRows ? 1 : '',
                        'layout' => 'block',

                        'sub_fields' => array(

                            // first one: column_size!

                            array(
                                'key' => $mainFieldNames[ 'field_main_column_size' ],
                                'label' => 'Breite',
                                'name' => 'column_size',
                                'type' => 'select',
                                'column_width' => '',
                                'choices' => $defaultFirstRowClasses,
                                'default_value' => $defaultFirstRowClassSelected,
                                'allow_null' => 0,
                                'multiple' => 0,
                            ) ,

                            // second one: Modules!

                            array(
                                'key' => $mainFieldNames[ 'field_module_id' ],
                                'label' => 'Spalteninhalt',
                                'name' => 'module',
                                'type' => 'flexible_content',
                                'column_width' => '',
                                'display' => 'row',
                                'layouts' => $availableModules,
                                'button_label' => 'Modul hinzufügen',
                                'min' => '',
                                'max' => '',
                            )

                        )

                    )

                ),

                'location' => $locationArray,

                'options' => array(

                    'position' => 'normal',
                    'layout' => 'default',
                    'hide_on_screen' => array(
                        0 => 'the_content',
                    )

                ),

                'menu_order' => 1

            )

        );

    }

    public function moduleAjax() {

        $module = ucfirst( $_POST['module'] );
        $method = ucfirst( $_POST['method'] );

        $moduleName = $module;

        $moduleController = $this->moduleFactory->getModuleInfo( $moduleName );

        header("Content-type: application/json");

        $methodName = 'ajax' . $method;

        if( method_exists($moduleController, $methodName ) ) {

            $json = $moduleController->{$methodName}();

            echo $json;

        } else {

            header('HTTP/1.0 400 Bad Request', true, 400);
            echo json_encode( array( 'status' => 400, 'message' => 'Bad or Invalid Request for requested Method: ' . $methodName.'()' ) );

        }

        exit;

    }

    private function getAllModuleFields() {

        $availableModules = array();

        $modulePath = NULL;

        if( $this->usingThemosis ) {

            $modulePath = themosis_path('admin') . 'dachcom/PageDesigner';

        } else {

            $modulePath = get_template_directory() . '/page_designer';

        }

        if( !is_dir( $modulePath ) ) {

            return $availableModules;

        }

        if ($handle = opendir( $modulePath ) ) {

            while (false !== ($folderName = readdir($handle))) {

                if ( $folderName == '.' || $folderName == '..' )
                    continue;

                $moduleFolder = $modulePath . '/' . $folderName;

                if( !is_dir( $moduleFolder ) )
                    continue;

                $moduleInfo = $this->moduleFactory->registerModule($folderName, $moduleFolder, $this->usingThemosis);

                if( !empty( $moduleInfo ) )
                    $availableModules[] = $moduleInfo->config->getFieldsForACF();

            }

            closedir($handle);

        }

        return $availableModules;

    }

}