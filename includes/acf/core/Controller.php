<?php

class PageDesignerController {

    var $actionInitialized = FALSE;

    /**
     * Auto populated by module loader
     * @var PageDesignerConfig
     */
    var $config = NULL;

    /**
     * Auto populated by module loader
     * @var PageDesignerModel
     */
    var $model = NULL;


    function __construct() { }

    /**
     * @param PageDesignerConfig $config
     *
     * @throws \Exception
     */
    function addConfig( \PageDesignerConfig $config ) {

        if( !is_null( $this->config ) ) {

            throw new Exception('Config already initialized');
        }

        $this->config = $config;
    }

    /**
     * @param PageDesignerModel $model
     *
     * @throws \Exception
     */
    function addModel( \PageDesignerModel $model ) {

        if( !is_null( $this->model ) ) {

            throw new Exception('Model already initialized');
        }

        $this->model = $model;

    }

    function addActions() {

        if( $this->actionInitialized )
            return FALSE;

        add_action('dachcom-page-designer/render/' . $this->config->getModuleName(), array($this, 'render' ) );

        $this->actionInitialized = TRUE;

    }

    /**
     * Usually this method will be called in child classes.
     */
    public function render() {

        return '';

    }

    protected function parseTemplate( $arguments ) {

        if( empty( $arguments ) ) {

            $arguments = array();

        }

        $col_size = get_sub_field('col_size');
        $responsive_adjustment = get_sub_field('dpd_responsive_adjustment');

        $col_class = dpd_get_page_designer_column_class( $col_size );

        //adjust responsive classes
        if( is_array( $responsive_adjustment ) && !empty( $responsive_adjustment ) ) {

            foreach( $responsive_adjustment as $ra_class) {

                if( empty($ra_class ) ) continue;
                $ns = substr( $ra_class, 0, 6);
                $col_class = preg_replace("/". $ns.'[^\s]+/i', $ra_class . ' ', $col_class);

            }

        }

        $arguments = array_merge(

            $arguments,
            array(

                'model' => $this->model,
                'config' => $this->config
            )

        );

        $moduleName = str_replace('_module','',$this->config->getModuleName() );

        //load Themosis View Framework.
        if( $this->config->getViewPath() === NULL ) {

            $htmlString = View::make('pagedesigner.' . $moduleName . '.' . $moduleName )->with($arguments)->render();

        } else {

            ob_start();

            try {

                if( !file_exists( $this->config->getViewPath() . $moduleName . '.php' ) ) {

                    $content = $this->doTemplateError('Template "' . $moduleName . '" does not exists');

                } else {

                    extract($arguments, EXTR_SKIP);

                    require $this->config->getViewPath() . $moduleName . '.php';

                    $content = ob_get_contents();

                }

            } catch(Exception $e ) {

                $content = $this->doTemplateError( $e->getMessage() );

            }

            ob_end_clean();

            $htmlString = $content;

        }

        return $this->config->getRenderDefaultWrapper() ? sprintf($this->config->getWrapperLayout(), $col_class, $htmlString) : $htmlString;

    }

    /**
     *
     * Get Sub Template. This Method only exists, if Themosis is not available.
     *
     * @param       $part
     * @param array $params
     *
     * @return string
     * @throws \Exception
     */
    public function getTemplatePart( $part, $params = array() ) {

        if( $this->config->getViewPath() === NULL) {

            throw new Exception('Cannot load template part when using the themosis fw.');

        }

        ob_start();

        extract($params, EXTR_SKIP);

        try {

            if( !file_exists( $this->config->getViewPath() . 'parts/' . $part . '.php' ) ) {

                $content = $this->doTemplateError('TemplatePart "' . $part . '" does not exists');

            } else {

                require_once $this->config->getViewPath() . 'parts/' . $part . '.php';
                $content = ob_get_contents();

            }

        } catch(Exception $e ) {

            $content = $this->doTemplateError( $e->getMessage() );

        }

        ob_end_clean();

        return $content;

    }


    private function doTemplateError( $message ) {

        return '<div class="error" style="color:red;"><strong>Error: </strong>' . $message . '</div>';

    }

}