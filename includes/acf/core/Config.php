<?php

class PageDesignerConfig {

    /**
     *
     * The Module Name. Must end with an _module
     * @var string
     */
	var $module_name = '';

    /**
     *
     * The Module Name. Comes without the _module suffix.
     * @var string
     */
    var $module_simple_name = '';

    /**
     * The human readable Name for your Module. It's shown in the PageDesigner Module Selector.
     * @var string
     */
    var $module_label = '';

    /**
     * @var string
     */
    var $module_description = '';

    /**
     * set a module prefix. must be very unique. eg field_m_text
     * @var string
     */
    private $module_prefix = '';

    /**
     * @var string
     */
    private $module_path = '';

    /**
     * @var null
     */
    var $view_path = NULL;

    /**
     * @var bool
     */
    var $render_default_wrapper = TRUE;

    /**
     * @var string
     */
    var $wrapper_layout = NULL;

    /**
     * @var array
     */
    var $allowed_columns = array();

    /**
     * @var int
     */
    var $default_column = null;

    /**
     * @var string
     */
    var $display = 'row';


    /**
     * All acf data for the module
     * @var array
     */
    var $sub_fields = array();

    /**
     * @var int
     */
    var $min = 0;

    /**
     * @var int
     */
    var $max = 0;

    function __construct() {

        /**
         * Set Default Wrapper Layout
         */
        $this->setWrapperLayout();

        /**
         * Setup Module Namespacing
         */
        $this->setupModule();

        /**
         * Load Subfields
         */
        $this->sub_fields = $this->setSubFields();


    }

    public function generateFieldKey( $name ) {

        return  $this->module_prefix . '_' . $name;

    }

    /**
     * Usually this Method will be overwritten by the child config classes.
     */
    public function setSubFields() {

        return array();

    }

    private function setupModule() {

        if( strpos( $this->module_name, '_module') ) {

            throw new Exception('Module cannot contain "_module" in Name.');

        }

        $moduleName = $this->module_name . '_module';

        $this->module_simple_name = $this->module_name;

        $this->module_name = $moduleName;

        $this->module_prefix = 'field_dpd_' . $this->module_simple_name;

    }

    /**
     * Add the default selection to the elements and return sub classes elements
     * @return mixed
     */
    public function getFieldsForACF() {

        if( !empty( $this->sub_fields ) ) {

            $sysFields = $this->generateDefaultModuleFields();

            //Add sysFields to the Beginning of SubFields
            array_unshift($this->sub_fields, $sysFields[0], $sysFields[1]);

        }

        $fields = array(

            'label'     => $this->module_label,
            'name'      => $this->module_name,
            'display'   => $this->display,
            'min'       => $this->min,
            'max'       => $this->max,

            'sub_fields' => $this->sub_fields

        );

        return $fields;

    }

    /**
     * you can use this method to add a default select field very quickly.
     */
    private function generateDefaultModuleFields( ) {

        $choices = array();

        $maxColumns = get_option('dpd_setting-theme_column');

        if( empty( $maxColumns ) )
            $maxColumns = 12;

        foreach( $this->allowed_columns as $size ) {

            if( $size > $maxColumns)
                continue;

            $choices[ dpd_col_int_to_string( $size ) ] = $size . '/' . $maxColumns;

        }

        if( is_null( $this->default_column ) ) {

            $defaultField = array_flip( $choices );
            $defaultField = array_pop( $defaultField );

        } else {

            $defaultField = dpd_col_int_to_string( $this->default_column );
        }

        $fields = array(

            array (

                'key' => $this->generateFieldKey('col_size'),
                'label' => 'Spaltenbreite',
                'name' => 'col_size',
                'type' => 'select',
                'column_width' => '',
                'instructions' => $this->module_description,
                'choices' => $choices,
                'default_value' => $defaultField,
                'allow_null' => 0,
                'multiple' => 0

            ),
            array (

                'key' => $this->generateFieldKey('responsive_adjustment'),
                'label' => '',
                'name' => 'dpd_responsive_adjustment',
                'type' => 'checkbox',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array (
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'choices' => $this->generateAdjustmentColSizes(),
                'default_value' => array (
                    '' => '',
                ),
                'layout' => 'vertical',

            )

        );

        return $fields;

    }

    private function generateAdjustmentColSizes(  ) {

        $prefixes = array('col-md', 'col-sm', 'col-ty', 'col-xs');

        $choices = array();

        $maxColumns = get_option('dpd_setting-theme_column');

        if( empty( $maxColumns ) )
            $maxColumns = 12;

        foreach( $prefixes as $p ) {

            foreach( $this->allowed_columns as $size ) {

                if( $size > $maxColumns)
                    continue;

                $choices[ $p . '-' . $size ] = $p . '/' . $size;

            }

        }

        return $choices;

    }

    private function setWrapperLayout() {

        $this->wrapper_layout = '<div class="%s">%s</div>';

    }

    public function __call($method, $args) {

        $_method = dpd_camelcase_to_underscore( $method, true );

        if (preg_match('~^(set|get)([A-Za-z_])(.*)$~', $_method, $matches)) {

            $property = strtolower($matches[2]) . $matches[3];
            if (!property_exists($this, $property)) {
                throw new Exception('Property ' . $property . ' not exists');
            }
            switch($matches[1]) {
                case 'set':
                    $this->checkArguments($args, 1, 1, $_method);
                    return $this->set($property, $args[0]);
                case 'get':
                    $this->checkArguments($args, 0, 0, $_method);
                    return $this->get($property);
                case 'default':
                    throw new Exception('Method ' . $_method . ' not exists');
            }
        }
    }

    public function get($property) {
        return $this->$property;
    }

    public function set($property, $value) {

        $property = dpd_camelcase_to_underscore( $property );

        $this->$property = $value;
        return $this;
    }

    protected function checkArguments(array $args, $min, $max, $methodName) {
        $argc = count($args);
        if ($argc < $min || $argc > $max) {
            throw new Exception('Method ' . $methodName . ' needs minimally ' . $min . ' and maximally ' . $max . ' arguments. ' . $argc . ' arguments given.');
        }
    }

}