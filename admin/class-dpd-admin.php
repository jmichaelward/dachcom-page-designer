<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://www.dachcom.com
 * @since      1.0.0
 *
 * @package    Dachcom_Page_Designer
 * @subpackage Dachcom_Page_Designer/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Dachcom_Page_Designer
 * @subpackage Dachcom_Page_Designer/admin
 * @author     Stefan Hagspiel <shagspiel@dachcom.ch>
 */
class Dachcom_Page_Designer_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

    /**
     * This ist required to register Plugin in Dachcom Repo
     *
     * @var DachcomPluginObserver_Connector
     */
    private $dpo_connector;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since       1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/admin.js', array( 'jquery' ), $this->version, false );

	}

    public function register_plugin() {

        $this->dpo_connector = new DachcomPluginObserver_Connector();
        $this->dpo_connector->connect_to_plugin_checker( $this->plugin_name, plugin_dir_path( dirname( __FILE__ ) ) . $this->plugin_name . '.php' );

    }

    /**
     * Register the settings page,
     * only if we don't have an Plugin Observer.
     *
     * @since    1.0.0
     */
    public function add_admin_menu() {

        add_options_page(
            'Dachcom Page Designer',
            __('Dachcom Page Designer', $this->plugin_name),
            'manage_options',
            'dachcom-page-designer',
            array($this, 'standalone_settings')
        );

    }

    function on_rendered_fields() {

        //only do this stuff when on a page where fields are rendered ....
        add_filter( 'admin_footer_text', array($this, 'add_module_float_js_vars') );

    }

    function update_wysiwyg_toolbar( $toolbars ) {

        if( ($key = array_search('fullscreen' , $toolbars['Full' ][1])) !== false )
        {
            unset( $toolbars['Full' ][1][ $key ] );
        }

        if( ($key = array_search('fullscreen' , $toolbars['Basic' ][1])) !== false )
        {
            unset( $toolbars['Basic' ][1][ $key ] );
        }

        return $toolbars;
    }

    function add_module_float_js_vars() {

        $amf_col_size_name = get_option('dpd_setting-inner_col_size_name');

        if(empty($amf_col_size_name))
            $amf_col_size_name = 'col_size';

        $amf_outer_col_size_name = get_option('dpd_setting-outer_col_size_name');

        if(empty($amf_outer_col_size_name))
            $amf_outer_col_size_name = 'column_size';

        $__module_float_js_vars = array(
            'dpd_col_size_name' => $amf_col_size_name,
            'dpd_outer_col_size_name' => $amf_outer_col_size_name,
        );

        $module_float_data = json_encode($__module_float_js_vars);

        echo '<input type="hidden" id="__module_float_js_vars" value="' . htmlspecialchars($module_float_data). '">';

    }

    public function include_acf_data() {

        require_once('includes/acf/page-designer-builder.php');

        new page_designer_builder( $this->plugin_name, plugin_dir_path( __FILE__ ), plugin_dir_url( __FILE__ ), $this->version );

    }

    /**
     * Add your Settings here, if want to extend the Dachcom Plugins
     */
   public function plugin_core_settings_api_init( ) {

       $subPage = NULL;

       if( !defined('USE_DC_PLUGIN_OBSERVER') || USE_DC_PLUGIN_OBSERVER == FALSE  ) {

           $subPage = 'dachcom-page-designer';

       } else {

           $subPage = 'dachcom-services';

       }

        /*
        * SECTIONS
        */

        add_settings_section(
            'dachcom-page-designer-section',
            __('Dachcom Page Designer Settings', 'dachcom-page-designer'),
            array($this, 'setting_section_callback'),
            $subPage
        );

        /*
		* SETTINGS FIELDS
		*/

        add_settings_field(
            'dpd_setting-theme_column',
           'Welches Spaltenlayout besitzt das Theme?',
            array($this, 'setting_theme_columns_callback'),
            $subPage,
            'dachcom-page-designer-section'
        );

        register_setting( 'dachcom-plugin-settings', 'dpd_setting-theme_column' );

        add_settings_field(
            'dpd_setting-outer_col_size_name',
            'Name des äusseren "Spalten-Feldes"',
            array($this, 'setting_outer_col_size_name_callback'),
            $subPage,
            'dachcom-page-designer-section'
        );

        register_setting( 'dachcom-plugin-settings', 'dpd_setting-outer_col_size_name' );

        add_settings_field(
            'dpd_setting-inner_col_size_name',
            'Name des inneren "Spalten-Feldes"',
            array($this, 'setting_inner_col_size_name_callback'),
            $subPage,
            'dachcom-page-designer-section'
        );

        register_setting( 'dachcom-plugin-settings', 'dpd_setting-inner_col_size_name' );

        add_settings_field(
            'dpd_setting-max_first_col_amount',
            'Wenn Sie das 1. Spalten Level deaktivieren möchten, aktivieren sie diese Option.',
            array($this, 'setting_max_first_col_amount_callback'),
            $subPage,
            'dachcom-page-designer-section'
        );

        register_setting( 'dachcom-plugin-settings', 'dpd_setting-max_first_col_amount' );

        add_settings_field(
            'dpd_setting-first_cols_allowed',
            'Welche Spaltenbreiten sind auf dem 1. Level erlaub?',
            array($this, 'setting_first_cols_allowed_callback'),
            $subPage,
            'dachcom-page-designer-section'
        );

        register_setting( 'dachcom-plugin-settings', 'dpd_setting-first_cols_allowed' );

    }

    function standalone_settings() {

        echo '<div class="wrap">';
            echo '<h2>'._e('Dachcom Settings', 'dachcom-plugin-observer') . '</h2>';
            echo '<form method="post" action="options.php">';
                settings_fields( 'dachcom-plugin-settings' );
                do_settings_sections( 'dachcom-page-designer' );
                submit_button( 'Einstellungen speichern' );
            echo '</form>';
        echo '</div>';

    }

    /* SECTIONS */
    function setting_section_callback() {
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/dpd-settings-section.php';
    }

    /* SETTINGS FIELDS */
    function setting_theme_columns_callback() {
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/dpd-setting-theme-columns.php';
    }
    function setting_outer_col_size_name_callback() {
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/dpd-setting-outer-col-size-name.php';
    }
    function setting_inner_col_size_name_callback() {
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/dpd-setting-inner-col-size-name.php';
    }
    function setting_max_first_col_amount_callback() {
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/dpd-setting-max-first-col-amount.php';
    }
    function setting_first_cols_allowed_callback() {

        $firstColumnsAllowed = array();
        $maxColumns = get_option('dpd_setting-theme_column');

        if( empty( $maxColumns ) )
            $maxColumns = 12;
        else $maxColumns = (int) $maxColumns;

        for($i=1;$i<$maxColumns+1;$i++)
            $firstColumnsAllowed[ $i ] = $i . '/' . $maxColumns;

        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/dpd-setting-first-cols-allowed.php';
    }


}
