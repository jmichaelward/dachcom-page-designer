<?php

class page_designer_builder extends acf_field {

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    private $plugin_dir_url;

    var $settings
    , $defaults
    , $domain
    , $lang
    , $notice_arg;


    function __construct( $plugin_name, $plugin_dir, $plugin_dir_url, $version ) {

        $this->plugin_name = $plugin_name;
        $this->plugin_dir = $plugin_dir;
        $this->plugin_dir_url = $plugin_dir_url;

        // vars
        $this->name = $this->plugin_name;
        $this->label = 'Page Designer';
        $this->category = __("Basic", $this->domain);
        $this->domain = 'dachcom-page-designer';
        $this->notice_args = array();

        // do not delete!
        parent::__construct();

        // settings
        $this->settings = array(

            'path'      => $plugin_dir_url,
            'dir'     => $plugin_dir_url,
            'version' => $version
        );

    }

    function input_admin_enqueue_scripts() {

        $theme_columns = get_option('dpd_setting-theme_column');

        if(!file_exists( $this->plugin_dir . 'css/visualizer_'. $theme_columns .'.css' )) {

            //class => updated, error
            $this->notice_args = array(
                'class' => 'error',
                'msg' => 'Es wurde keine CSS mit dem Namen "visualizer_'. $theme_columns .'.css" gefunden.'
            );

            add_action( 'admin_notices', array($this, 'admin_notices'));

        } else {

            wp_enqueue_style( 'extend_acf_flexible_field_visualizer_' . $theme_columns . '_css', $this->plugin_dir_url . 'css/visualizer_'. $theme_columns .'.css', array(), $this->settings['version'] );

        }

        wp_enqueue_style( 'extend_acf_flexible_field_visualizer_default_css', $this->plugin_dir_url . 'css/admin.css', array(), $this->settings['version'] );
        wp_enqueue_script( 'extend_acf_flexible_field_visualizer_js', $this->plugin_dir_url .'js/admin.js', array(), $this->settings['version'] );

    }

    function admin_notices(){

        if(!current_user_can('update_core'))
            return;

        echo '<div class="'. $this->notice_args['class'] .'">';
            echo '<p>';
                echo '<b>'. $this->label .':</b> ' . $this->notice_args['msg'];
            echo '</p>';
        echo '</div>';
    }


}