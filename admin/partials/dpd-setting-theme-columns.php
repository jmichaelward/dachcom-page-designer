<?php $option = get_option('dpd_setting-theme_column'); ?>

<select name="dpd_setting-theme_column">
    <option value="null">ohne</option>
    <option value="4"<?php echo selected( $option, '4', false); ?>>4-Spaltig</option>
    <option value="6"<?php echo selected( $option, '6', false); ?>>6-Spaltig</option>
    <option value="8"<?php echo selected( $option, '8', false); ?>>8-Spaltig</option>
    <option value="10"<?php echo selected( $option, '10', false); ?>>10-Spaltig</option>
    <option value="12"<?php echo selected( $option, '12', false); ?>>12-Spaltig</option>
</select>
