<?php $options = get_option('dpd_setting-first_cols_allowed'); //amf_first_cols_allowed ?>

<?php foreach( $firstColumnsAllowed as $col => $label ) { ?>

    <label>
        <input type="checkbox" value="<?php echo (int) $col; ?>" name="dpd_setting-first_cols_allowed[<?php echo $col; ?>]"<?php echo checked( (isset($options[$col]) ? $options[$col] : null), $col, false); ?> />
        <?php echo $label; ?>
    </label><br>

<?php } ?>