// PageDesigner 1.7, written by Stefan Hagspiel

var PageDesigner;

( function( $ ) {

    $(document).ready(function() {

        /**
         * Add Selector Fix, if we are using the themosis fw.
         */
        if( $('#_themosisPageTemplate-id').length > 0 ) {

            $(document).on('change', '#_themosisPageTemplate-id', function(){

                var page_template = $(this).val();

                acf.ajax.update( 'page_template', page_template ).fetch();

            });

        }

        PageDesigner.init();

    });

    PageDesigner = {

        $mainContainer : null,
        $pageDesigner : null,

        outerColSizeName : null,
        colSizeName : null,
        currentScrollPos : null,

        editorIsActive : false,

        backup : null,
        meTimer : null,

        /**
         * How long to wait, until 1. Level Row Positions should be updated?
         */
        updateRowDelay : 50,

        busy : false,

        init : function() {

            var _root = this;

            this.$mainContainer = $('#wpbody-content');
            this.$pageDesigner = this.$mainContainer.find( '#acf-group_page_designer' );
            this.$pageDesignerInside = this.$pageDesigner.find( '.inside' );

            var moduleFloatJsVars = jQuery.parseJSON( $('input#__module_float_js_vars').val() );

            if( !moduleFloatJsVars )
                return;

            this.outerColSizeName = moduleFloatJsVars.dpd_outer_col_size_name;
            this.colSizeName = moduleFloatJsVars.dpd_col_size_name;
            this.currentScrollPos = null;

            if( this.$pageDesigner.length == 0 )
                return;

            this.setupVisualBoard();

            $(window).load( function() {

                _root.initEvents();
                _root.initColSelectors();

            });

        },

        /**
         * Initialise Page Designer
         */
        setupVisualBoard : function() {

            //append preloader
            this.$pageDesignerInside.addClass('acf-cursor-loading');
            this.$pageDesignerInside.before('<div class="dpd-acf-modue-float-preloader"></div>');

        },

        /**
         * Init Column Selector
         */
        initColSelectors : function() {

            var _root = this;

            /* 1.level */
            var outer_col_selectors = _root.$pageDesigner.find('.acf-table tr:not(.acf-clone) [data-name="' + _root.outerColSizeName + '"] select');

            //.acf-table tbody > .acf-row:not(.acf-clone)
            outer_col_selectors.each(function(i, outer_col_selector) {

                var valClass = $(outer_col_selector).val(),
                    repeaterRow = $(outer_col_selector).closest('.acf-row');

                repeaterRow.addClass( 'acf-row ' + valClass );

            });

            /* 2.level */
            var col_selectors = _root.$pageDesigner.find('.acf-flexible-content > .values > .layout > .acf-fields > div[data-name="' + _root.colSizeName + '"] select');

            col_selectors.each(function(i, col_selector) {

                $sel = $( col_selector );
                $sel.closest('.layout').addClass( $sel.val() );

                _root.prepareTableRow( $sel );

            });

            //set pre init state
            _root.$pageDesignerInside.addClass('pre-initialized');

            setTimeout($.proxy(_root.updateRowPositionClasses, _root), _root.updateRowDelay  );


        },

        /**
         * Add Events to all the Elements.
         */
        initEvents : function() {

            var _root = this;

            //add resize event;
            $(window).on('resize', $.proxy( _root.onResize, _root ) );

            //disable save post while editor is open!!
            $('#post').on('submit', function( ev ) {

                if( _root.editorIsActive )
                    ev.preventDefault();

            });

            //set drop events
            this.$pageDesigner.on( 'sortupdate', '.ui-sortable', function( ev ) {

                setTimeout($.proxy(_root.updateRowPositionClasses, _root), _root.updateRowDelay  );

            });

            this.$pageDesigner.on('mouseenter mouseleave', '.acf-row.tiny-mode', function( ev ) {

                if(  _root.editorIsActive )
                    return false;

                var $el = $(this),
                    elPos = $el.position(),
                    mType = ev.type,
                    parent = $el.closest('table.acf-table');

                //prevent re-entering
                if( mType == 'mouseenter' && $el.hasClass('visible')) {
                    return;
                }

                if( $el.data('init-height') == undefined )
                    $el.data('init-height', $el.outerHeight() );

                var activeLayouts = $el.find('.values > .layout').length,
                    minHeightVal = 0;

                if( activeLayouts == 0 )
                    minHeightVal = $el.data('init-height');

                if( mType == 'mouseenter') {

                    $el.data('tiny-action', true );

                    _root.meTimer = setTimeout(function() {

                        $el.before( $('<tr class="spacer"></tr>')
                            .css({

                                'marginLeft' : $el.css('marginLeft'),
                                'marginRight' : $el.css('marginRight'),
                                'width' : $el.outerWidth() - 1,
                                'height' : $el.outerHeight(),
                                'float' : 'left'
                            })
                        );

                        $el.css({

                            position : 'absolute',
                            zIndex : 10001 /* same as visualiser */,
                            minHeight : minHeightVal,
                            left : elPos.left,
                            top : elPos.top

                        }).animate( { width : 400 }, 150, 'linear', function () {

                                $el.addClass('visible');
                                parent.css( { 'height' :  $el.outerHeight() + 50 } );

                            }
                        );

                    }, 150);

                } else {

                    $el.data('tiny-action', false );

                    clearTimeout( _root.meTimer );

                    setTimeout(function() {

                        $el.prev('tr.spacer').remove();

                        $el.stop().css({

                            position : '',
                            minHeight : '',
                            zIndex : '',
                            left : 'initial',
                            top : 'initial',
                            width : ''

                        }).removeClass('visible');

                        parent.css( { 'height' : '' } );
                        _root.$pageDesigner.trigger('row-position-updated');

                    }, 150);

                }

            });

            //open field
            this.$pageDesigner.on('click', '.btn-open-visualizer', function(ev) {

                _root.editorIsActive = true;
                _root.$pageDesigner.addClass('editor-active');

                var $layout = $(this).closest('.layout'),
                    $layoutHandler = $layout.find('.acf-fc-layout-handle');

                //make storepoint for undo!
                _root.createBackup( $layout );

                $layout.data('hasFirst', $layout.hasClass('first') ).removeClass('first').addClass('activeEditor');
                $layoutHandler.removeClass('acf-fc-layout-handle').addClass('acf-fc-layout-handle-disabled');

                //prepend the upper close button
                $layoutHandler.append( '<a href="#" class="visualizer-close media-modal-close"><span class="media-modal-icon"></span></a>' );

                //disableScroll for wp
                _root.currentScrollPos = $(document).scrollTop();
                $('body').addClass('disableScroll');

                //soft tiny mode
                _root.$pageDesigner.find('.acf-row.tiny-mode.visible').addClass('soft');

                //add"popup"-div element
                _root.$mainContainer.append('<div id="dpd-visualizer-popup"></div>');

                _root.adjustVisualEditorPosition();

                //trigger an google map resize!
                if( $layout.find('.acf-field-google-map').length > 0 ) {

                    acf.do_action('show_field', $layout.find('.acf-field-google-map') );

                }

            });

            //close field
            this.$pageDesigner.on('mouseup', '.btn-close-visualizer, .visualizer-close', function(ev) {

                _root.editorIsActive = false;
                _root.$pageDesigner.removeClass('editor-active');

                var $layout = $(this).closest('.layout');

                var restoreBackup = false;

                //do backup action if required.
                if( _root.backup != null & ( $(ev.target).hasClass('visualizer-close') || $(ev.target).hasClass('media-modal-icon') ) ) {

                    restoreBackup = true;

                    $layout.replaceWith( _root.getBackup() );

                    _root.backup = null;

                }

                //there is already a preview-field, get next one
                _root.preparePreview( $layout.find('table.acf-table > tbody') );

                $layout.removeClass('activeEditor');
                $layout.find('.acf-fc-layout-handle-disabled').removeClass('acf-fc-layout-handle-disabled').addClass('acf-fc-layout-handle');

                if( $layout.data('hasFirst') )
                    $layout.addClass('first');

                //soft tiny mode
                _root.$pageDesigner.find('.acf-row.tiny-mode.visible').removeClass('soft');

                //remove "resize-event"-handler from window
                $layout.css('margin-top', '');

                //remove "popup"-wrapper
                $('#dpd-visualizer-popup, .visualizer-close').remove();

                //enable scroll for wp
                $('body').removeClass('disableScroll');
                $(document).scrollTop( _root.currentScrollPos );

                if( !restoreBackup  )
                    $layout.stop().css('background-color', '#47A365').animate({ backgroundColor: '#ffffff'}, 550);

            });

            // open up --> add-image, add-file
            $(document).on('click', '.add-file, .add-image', function(ev){

                if( $('.visualizeEdit').length == 0 )
                    return;

                $('.visualizeEdit').addClass('temporaryHidden');
                $('#dpd-visualizer-popup').addClass('temporaryHidden');

            });

            $(document).on('click', '.media-toolbar-primary', function(ev){

                if( $('.visualizeEdit').length == 0 )
                    return;

                if( $(ev.target).hasClass('media-button-select') )
                    $('.temporaryHidden').removeClass('temporaryHidden');

            });

            //on close --> add-image, add-file
            $(document).on('click', '.media-modal-close', function(ev){

                if( $('.visualizeEdit').length == 0 )
                    return;

                $('.temporaryHidden').removeClass('temporaryHidden');

            });

            $(document).keyup(function(e) {

                if (e.keyCode == 27 && this.editorIsActive != false && $('.activeEditor').length > 0) {

                    //get open layout, and close it
                    $('.activeEditor').find('.visualizer-close').trigger('mouseup');

                }
            });

            /**
             * Acf Action: ready, append
             */
            acf.add_action('ready append', $.proxy(this.onFieldAdd, this) );

            /**
             * Acf Action: refresh
             *
             * Attention: Because acf triggers refresh only when field toggles to open state (that's ok, btw),
             * we need to trigger the field refresh also while closing because of the changing row layout heights
             * @fixme
             */
            acf.add_action('refresh', $.proxy(this.onFieldRefresh, this) );

            /**
             * Acf Action: remove
             */
            acf.add_action('remove', $.proxy(this.onFieldRemove, this) );

            /**
             * Acf Action: add field error
             */
            acf.add_action('add_field_error', this.onFieldAddError );

            /**
             * Acf Action: remove field error
             */
            acf.add_action('remove_field_error', this.onFieldRemoveError );

            this.$pageDesigner.on('blur', '.acf-flexible-content .acf-fc-popup .focus', function( ev ) {

                setTimeout( function() {

                    $( this ).closest('.acf-row').css( { overflow : '' });
                    $( this ).closest('.acf-table-wrap').css( { overflow : '' });

                }, 40);

            });

            //1. level selector
            this.$pageDesigner.on('change', '[data-name="' + _root.outerColSizeName + '"]', function( ev ) {

                var select = $(this).find('select'),
                    repeaterRow = select.closest('.acf-row');

                repeaterRow.removeClass(function (index, css) { return (css.match(/(^|\s)\S+Col/g) || []).join(' '); }).addClass(  select.val() );

                setTimeout($.proxy(_root.updateRowPositionClasses, _root), _root.updateRowDelay  );


            });

            //2. level selector
            this.$pageDesigner.on('change', '[data-name="' + _root.colSizeName + '"]', function( ev ) {

                var layout = $(this).closest('.layout'),
                    table_rows = $(layout).find('.acf-fields > .acf-field');

                $(table_rows).each(function(k, table_row ) {

                    //its the col-size selector
                    if( k == 0 ) {

                        var $col_selector = $(table_row).find('select');

                        if($col_selector == null)
                            return;

                        var selected = $col_selector.find('option:selected');
                        $col_selector.find('option').attr('selected', false );
                        selected.attr('selected', true );

                        //change class name
                        $(this).closest('.layout').removeClass(function (index, css) { return (css.match(/(^|\s)\S+Col/g) || []).join(' '); }).addClass( $col_selector.val() );

                        setTimeout($.proxy(_root.updateRowPositionClasses, _root), _root.updateRowDelay  );


                    }

                });

            });

            this.$pageDesigner.on('row-position-updated', function( ev ) {

                //ok, add pre inital state.
                _root.$pageDesignerInside.addClass('initialized');
                _root.$pageDesignerInside.removeClass('acf-cursor-loading pre-initialized');
                _root.$pageDesignerInside.prev('.dpd-acf-modue-float-preloader').remove();

                var col_selectors = _root.$pageDesigner.find('.acf-flexible-content > .values > .layout > .acf-fields > div[data-name="' + _root.colSizeName + '"] select');

                col_selectors.each( function(i, col_selector ) {

                    /* here we check, if the layout is to small so we hide everything and toggle it with mo/me */
                    var $el = $( col_selector ).closest('.acf-row'),
                        rW = $el.outerWidth(),
                        tW = $el.find('.acf-fields').first().outerWidth();

                    if( $el.data('tiny-action' ) !== true ) {

                        if( tW > rW )
                            $el.addClass('tiny-mode');
                        else
                            $el.removeClass('tiny-mode');

                    }

                });

            });

            //responsive adjustments
            this.$pageDesigner.on('change', 'input.adjust-responsive-toggler', function( ev ) {

                var $el = $(this),
                    $acf_input = $el.closest('.acf-input'),
                    $wrapper = $acf_input.find('.responsive-range-wrapper');

                $wrapper.toggle();

                if( !$wrapper.is('visible') ) {

                    $wrapper.find('select').each( function() {
                        $(this).prop("selectedIndex", 0).change();
                    });

                }

            });

            this.$pageDesigner.on('change', 'select.responsive-range-selector', function( ev ) {

                //first reset all cols of range
                var $el = $(this),
                    $acf_input = $el.closest('.acf-input'),
                    $col_list = $acf_input.find('.acf-checkbox-list'),
                    range_name = $el.find(':selected').data('range');

                var $rel_checkbox = $col_list.find('input[value="' + $(this).val() + '"]');

                $col_list.find('input[value^="' + range_name + '"]').prop('checked', false);

                if( $rel_checkbox.length == 1)
                    $rel_checkbox.prop('checked', true);


            });

        },

        onResize : function() {

            var _root = this;

            if( _root.editorIsActive == false)
                return;

            _root.adjustVisualEditorPosition();

        },


        /*

            METHODS

        */


        adjustVisualEditorPosition: function() {

            var _root = this;

            var layout = _root.$mainContainer.find('.activeEditor'),
                layoutHeight = layout.height();

            layout.css('margin-top', - (layoutHeight/2));

        },

        preparePreview : function( $tbody ) {

            var $firstl = $tbody.find('div.acf-field:gt(1)').first(),
                fieldType = $firstl.data('type'),
                $previewField = $tbody.find('.dpd-preview-field');

            var cleanTextVal = '';

            //create field, if not available.
            if( $previewField.length == 0 ) {

                $previewField = $('<div class="acf-field dpd-preview-field"> <div class="inner"></div></div>').appendTo($tbody);

            }

            if( fieldType == 'wysiwyg' ) {

                var textVal = $firstl.find('textarea').val();

                cleanTextVal = this.stripTags(textVal);

                if( cleanTextVal.length != 0 ) {

                    var cleanTextValLength = cleanTextVal.length;
                    cleanTextVal = cleanTextVal.substring(0, 35);

                    if( cleanTextVal.length < cleanTextValLength )
                        cleanTextVal = cleanTextVal + ' ...';
                }

            } else if( fieldType == 'text' ) {

                var textVal = $firstl.find('input').val();

                cleanTextVal = this.stripTags(textVal);

                if( cleanTextVal.length != 0 ){

                    var cleanTextValLength = cleanTextVal.length;

                    cleanTextVal = cleanTextVal.substring(0, 25);
                    if( cleanTextVal.length < cleanTextValLength )
                        cleanTextVal = cleanTextVal + ' ...';
                }

            } else if( fieldType == 'select' ) {

                var textVal = $firstl.find('select option:selected').text();
                cleanTextVal = textVal;

            } else if( fieldType == 'image' ) {

                var img = $firstl.find('.acf-input img');
                if( img.length > 0 && img.attr('src') !== '' ){
                    cleanTextVal = '<img src="' + img.attr('src') + '" style="width:30px;">';

                }

            }

            if( cleanTextVal == '' || cleanTextVal.length == 0 )
                cleanTextVal = 'Keine Vorschau verfügbar.';

            $previewField.find('.inner').html( cleanTextVal );

        },

        prepareTableRow : function( $selector ) {

            var $tbody = $selector.closest('div.acf-fields');

            this.preparePreview( $tbody );
            this.addResponsiveAdjustment( $tbody );

            var newTableRowEdit = ' \
				<div class="visualizer-button"> \
					<span> \
						<input type="button" class="button button-default btn-open-visualizer" value="Inhalt bearbeiten"> \
					</span> \
				</div>';

            $selector.after(newTableRowEdit);

            var newTableRowClose = ' \
				<div class="acf-field visualizer-close-row"> \
					<div class="acf-label"></div> \
					<div class="acf-input"> \
                        <td class="save-btn"> \
                            <input type="button" class="button button-primary btn-close-visualizer" value="Speichern & Schließen"> \
                        </td> \
                        <td><span class="acf-label"> \
                            <p class="description">Hinweis: Wenn Sie das Fenster über ESC oder das Schließen-Symbol rechts oben schließen, wird der Inhalt nicht gespeichert.</p> \
                        </span></td> \
					</div> \
				</div>';

            $tbody.append(newTableRowClose);

        },

        addResponsiveAdjustment : function( $el ) {

            var $sel = $el.find('div[data-name="dpd_responsive_adjustment"]');

            if( $sel.length == 0)
                return;

            var $acf_input = $sel.find('.acf-input'),
                $col_list = $sel.find('.acf-checkbox-list');

            var checked = $col_list.find('input:checked').length > 0 ? 'checked' : '';
            $acf_input.append(

                $('<label />').text('Responsive Adjustierung vornehmen')
                    .prepend( $('<input type="checkbox" value="adjust_responsive" class="adjust-responsive-toggler" ' + checked + '>')
                )
            );

            var $el_wrap = $('<div />').addClass('responsive-range-wrapper').appendTo( $acf_input);

            if( checked !== 'checked')
                $el_wrap.hide();

            $.each(['xs','ty','sm','md'], function(i,range) {

                $sel = $('<select />')
                    .addClass('responsive-range-selector');

                $sel.append( $('<option />')
                    .val('null' ).attr('data-range', 'col-' + range )
                    .text(' Keine Anpassung (' + range.toUpperCase() + ')' ))

                $.each([6,7,8,9,10,11,12], function(i,colID) {

                    var value = 'col-' + range + '-' + colID,
                        sub_checked = $col_list.find('input[value="' + value + '"]:checked').length > 0;

                    $sel.append( $('<option />')
                        .val( value )
                        .attr('data-range', 'col-' + range)
                        .prop('selected', sub_checked)
                        .text( range.toUpperCase() + ' / ' + colID) );

                });


                $el_wrap.append( $sel );

            });

        },


        /*

            ACF HOOKS

        */

        /**
         *
         * @param $el
         *  @see acf.fields.flexible_content.add
         */
        onFieldAdd : function( $el ) {

            var col_size_fields = $el.find("[data-name='col_size']:not(.clones tr)");
            var column_size_fields = $el.find('[data-name="column_size"]:not(.clones tr)');

            if( col_size_fields.length == 0 && column_size_fields.length == 0)
                return;

            //find outer column selectors
            if( column_size_fields.length > 0 ) {

                var col_size = $(column_size_fields[0]).find('select').val();
                $el.addClass( col_size );

            }

            //find inner column selectors
            if( col_size_fields.length > 0 ) {

                var $selector = $(col_size_fields[0]).find('select');
                var col_size = $selector.val();

                $el.closest('.layout').addClass( col_size );
                this.prepareTableRow( $selector );

            }

            setTimeout($.proxy(this.updateRowPositionClasses, this), this.updateRowDelay  );

        },

        /**
         *
         * @param $field
         *  @see acf.fields.flexible_content.toggle
         */
        onFieldRefresh : function( $field ) {

            setTimeout($.proxy(this.updateRowPositionClasses, this), this.updateRowDelay  );

        },

        /**
         *
         * @param $field
         * @see acf.fields.flexible_content.remove
         */
        onFieldRemove : function( $field ) {

            setTimeout($.proxy(this.updateRowPositionClasses, this), this.updateRowDelay  );

        },

        /**
         *
         * @param $field
         * @see acf.validation.add_error
         */
        onFieldAddError : function( $field ) {

            $field.closest('.layout').addClass('has-error');

        },

        /**
         *
         * @param $field
         * @see acf.validation.remove_error
         */
        onFieldRemoveError : function( $field ) {

            //$field.closest('.layout').removeClass('has-error');

        },


        /*

            BACKUP HELPER

        */


        createBackup : function( $layout ) {

            var _root = this;

            var tb = {};

            acf.get_fields({ type : 'wysiwyg'}, $layout).each(function(){

                var taField = acf.fields.wysiwyg.doFocus( $(this) );

                var ed = tinyMCE.get( taField.o.id ),
                    isHtmlMode = false;

                if( $(this).find('.wp-editor-wrap').hasClass('html-active') )
                    isHtmlMode = true;

                if( ed != null) {

                    tb[ taField.o.id ] = isHtmlMode ? tinyMCE.DOM.get( taField.o.id ).value : ed.getContent();

                    taField.disable();

                }

            });

            _root.backup = $layout.clone( true, true );

            _root.backup.find('textarea').each( function() {

                if( tb[ $(this).attr('id') ] )
                    $(this).val( tb[ $(this).attr('id') ] );

            });

            delete tb;

            acf.get_fields({ type : 'wysiwyg'}, $layout).each(function() {

                var taField = acf.fields.wysiwyg.doFocus( $(this) );

                //change default size.
                var isHtmlMode = false;

                if( $(this).find('.wp-editor-wrap').hasClass('html-active') )
                    isHtmlMode = true;

                if( !isHtmlMode ) {

                    taField.enable();

                    var ed = tinyMCE.get( taField.o.id );

                    if ( ed != null )
                        ed.theme.resizeTo('100%', '150');

                }

            });

        },

        getBackup : function() {

            //before we return data, we need to update tinymce's values.
            this.backup.find('textarea').each( function() {

                var ed = tinyMCE.get( $(this).attr('id') );

                if( ed != null)
                    ed.execCommand('mceSetContent', false, $(this).val() );

            });

            return this.backup;

        },


        /*

            HELPER

        */


        stripTags : function(input, allowed) {

            allowed = (((allowed || '') + '')
                .toLowerCase()
                .match(/<[a-z][a-z0-9]*>/g) || [])
                .join(''); // making sure the allowed arg is a string containing only tags in lowercase (<a><b><c>)
            var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi,
                commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
            return input.replace(commentsAndPhpTags, '')
                .replace(tags, function($0, $1) {
                    return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
                });
        },

        updateRowPositionClasses : function(  ) {

            var _root = this;

            if( this.busy )
                return;

            this.busy = true;

            //get all rows (1. level)
            this.$pageDesigner.find('.inside > .acf-field-repeater > .acf-input > .acf-repeater > table > tbody > .acf-row:not(.acf-clone)').each(function( k ) {

                var $tr = $(this);

                $tr.removeClass('first');

                //its the first row!
                if( Math.round( $tr.position().left ) < 20) {

                    $tr.addClass('first');

                } else {

                    var thisRowElements = $tr.prevUntil('[class*="first"]').andSelf().prev('tr.first').andSelf();

                    thisRowElements.height('auto');

                    var maxHeight = Math.max.apply(null,thisRowElements.map(function () {
                        return $(this).height();
                    }).get());

                    if( maxHeight > 0 )
                        thisRowElements.height( maxHeight );

                }

                //get all nested modules
                $tr.find('div.values div.layout').each( function( i, module ) {

                    var $m = $(module);

                    $m.removeClass('first');

                    if( Math.round( $m.position().left ) <= 0)
                        $m.addClass('first');

                });

            });

            this.$pageDesigner.trigger('row-position-updated');

            //buffer overrun protection
            setTimeout( function() { _root.busy = false;}, 100 );

        }

    };

}(jQuery));