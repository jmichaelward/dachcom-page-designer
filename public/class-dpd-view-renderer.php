<?php 

/*
 * we have a double layer:
 * 
 * 1. layer: page_designer columns
 * 2. layer: module
 */


/*
 * $_dpd_module_type_counter: use this global var to access the count of each module within the flexible content field 
 */

class Dachcom_Page_Designer_RenderEngine {
		
	private $mainColumnCurrentSize = NULL;
		
	public function render( $params = array() ) {
	
		global $post, $_dpd_module_type_counter;
		
		$default = array(

			'render_first_level_rows' => TRUE,
			'render_second_level_rows' => TRUE

		);
		
		$params = array_merge( $default, $params );

		if( !have_rows('page_designer') )
			return '';

		$layout_sizes = self::setupLayoutCols();

		$_dpd_module_type_counter = array();
		
		$block_counter = 0;
		$col_counter = 0;
		
		do_action('dachcom-page-designer/before-do-first-level');

        $str = '';

		while( have_rows('page_designer' ) ) {
			
			the_row();

			$currentFirstLevelBlock = $layout_sizes[ $block_counter ];

			$column_size = apply_filters('dachcom-page-designer/current-first-level-column-class', get_sub_field('column_size'), $block_counter, $col_counter );
			
			$this->mainColumnCurrentSize = $column_size;
				
			$colClass = dpd_get_page_designer_column_class( $column_size );

			if( $currentFirstLevelBlock['open'] === TRUE && $params['render_first_level_rows'] == TRUE) {

                $str .= '<div class="row first-level">';
			
			}

			$str .= '<div class="' . $colClass . '">'; //start 1. level col-xy
		
				//now show second level!
				if( have_rows('module', $post->ID) ) {
							
					$sub_block_counter = 0;

					while( have_rows('module' ) ) {
	
						the_row();

                        $currentsecondLevelBlock = $layout_sizes[ $block_counter ]['modules'][ $sub_block_counter ];

						if( $currentsecondLevelBlock['open'] === TRUE && $params['render_second_level_rows'] == TRUE ) {

                            $str .= '<div class="row second-level">';
							
						}
						
						$module_name = get_row_layout();

                        do_action('dachcom-page-designer/before-render/' . strtolower($module_name));

                        if( isset( $_dpd_module_type_counter[ $module_name ] ) )
                            $_dpd_module_type_counter[ $module_name ]++;
                        else
                            $_dpd_module_type_counter[ $module_name ] = 1;

                        $module_string = '<div class="module ' . $module_name . '">';

                        /**
                         * @string dpd_core_module
                         */

                        ob_start();

                        do_action('dachcom-page-designer/render/' . strtolower($module_name) );

                        $module_string  .= ob_get_clean();

                        $module_string .= '</div>';

                        $str .= $module_string;

                        if( $currentsecondLevelBlock['close'] === TRUE && $params['render_second_level_rows'] == TRUE ) {

                            $str .= '</div>';

                        }

                        $sub_block_counter++;
						
					}
				
				}
					
				//end 1. level col-xy
                $str .= '</div>';
				
			//close 1. scond level col
			if( $currentFirstLevelBlock['close'] === TRUE && $params['render_first_level_rows'] == TRUE) {

                $str .= '</div>';
			
			}
				
			$col_counter += dpd_calculate_row_count( $column_size );
			$block_counter++;
			
		}

        return $str;

	}
	
	private static function setupLayoutCols() {
		
		global $post;
		
		$max_columns = (int) get_option('dpd_setting-theme_column' );
		$page_designer_rows = get_field('page_designer', $post->ID);
		
		$moduleLayout = array();
		
		//1. level
		$availableFirstLevelRows = $max_columns;
			
		foreach( $page_designer_rows as $i1 => $pd_row ) {
			
			$size1 = dpd_calculate_row_count( $pd_row['column_size']);
			$lastSize = isset( $page_designer_rows[ $i1-1 ] ) ? dpd_calculate_row_count( $page_designer_rows[ $i1-1 ]['column_size']) : FALSE;
			$nextSize = isset( $page_designer_rows[ $i1+1 ] ) ? dpd_calculate_row_count( $page_designer_rows[ $i1+1 ]['column_size']) : FALSE;
				
			$module_rows = $pd_row['module'];
			
			$open = FALSE;
			$close = FALSE;
			
			if( $i1 == 0 ) {
				
				$open = TRUE;
			
			}
			
			if( ( ( $availableFirstLevelRows <= 0  ) || ( $availableFirstLevelRows - $size1 < 0 ) ) ) {
					
				$open = TRUE;
				$availableFirstLevelRows = $max_columns;
					
			}
			
			$availableFirstLevelRows -= $size1;
			
			if( $availableFirstLevelRows <= 0 || $nextSize === FALSE || ( $availableFirstLevelRows - $nextSize ) < 0)
				$close = TRUE;
			
			$moduleLayout[] = array('open' => $open, 'close' => $close, 'size' => $size1, 'modules' => array() );
			
			//2. level
			$availableRows = $max_columns;
			
			if( empty( $module_rows ) )
				continue;

			foreach( $module_rows as $i2 => $m_row ) {
				
				$size2 = dpd_calculate_row_count( $m_row['col_size'] );
				$nextSize = isset( $module_rows[ $i2+1] ) ? dpd_calculate_row_count( $module_rows[ $i2+1 ]['col_size']) : FALSE;
				
				$open = FALSE;
				$close = FALSE;
				
				if( $i2 == 0 ) {

					$open = TRUE;		
					
				}
				
				if( ( ( $availableRows <= 0  ) || ( $availableRows - $size2 < 0 ) ) ) {
					
					$open = TRUE;
					$availableRows = $max_columns;
					
				}

				$availableRows -= $size2;
				
				if( $availableRows <= 0 || $nextSize === FALSE || ( $availableRows - $nextSize ) < 0)
					$close = TRUE;

				$moduleLayout[ $i1 ][ 'modules' ][] = array('open' => $open,  'close' => $close,  'size' => $size2);
				
			}
			
		}

		
		return $moduleLayout;
		
	}
}