<?php

/**
 * The plugin bootstrap file
 *
 *
 * @link              http://www.dachcom.com
 * @since             1.0.0
 * @package           Dachcom_Page_Designer
 *
 * @wordpress-plugin
 * Plugin Name:       Dachcom Page Designer
 * Plugin URI:        http://www.dachcom.com
 * Description:       Generates a Page Designer
 * Version:           1.8.0
 * Author:            DACHCOM DIGITAL : Stefan Hagspiel
 * Author URI:        http://www.dachcom.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       dachcom-page-designer
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-dpd-activator.php
 */
function activate_dachcom_page_designer() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-dpd-activator.php';
	Dachcom_Page_Designer_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-dpd-deactivator.php
 */
function deactivate_dachcom_page_designer() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-dpd-deactivator.php';
	Dachcom_Page_Designer_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_dachcom_page_designer' );
register_deactivation_hook( __FILE__, 'deactivate_dachcom_page_designer' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-dpd.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_dachcom_page_designer() {

	$plugin = new Dachcom_Page_Designer();
	$plugin->run();

}

run_dachcom_page_designer();